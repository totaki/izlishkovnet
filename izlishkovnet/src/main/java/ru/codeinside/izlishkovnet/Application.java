package ru.codeinside.izlishkovnet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import ru.codeinside.izlishkovnet.config.ConfigService;
import ru.codeinside.izlishkovnet.models.Farmer;
import ru.codeinside.izlishkovnet.models.Product;
import ru.codeinside.izlishkovnet.models.User;
import ru.codeinside.izlishkovnet.repositories.FarmerRepository;
import ru.codeinside.izlishkovnet.repositories.ProductRepository;
import ru.codeinside.izlishkovnet.repositories.UserRepository;
import ru.codeinside.izlishkovnet.services.FarmerService;
import ru.codeinside.izlishkovnet.services.ProductService;
import ru.codeinside.izlishkovnet.services.UserService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@Import({ConfigService.class})
public class Application implements CommandLineRunner
{
    @Autowired
    UserService userService;
    @Autowired
    ProductService productService;
    @Autowired
    FarmerService farmerService;
    @Autowired
    FarmerRepository farmerRepository;
    @Autowired
    ProductRepository productRepository;


    Product product;

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... strings) throws Exception
    {
        productSet();
    }

    private void productSet(){

/*
        String file="C:\\Users\\DiMan\\Desktop\\addProduct.txt";
        StringBuilder sb = new StringBuilder();
        ArrayList<String> str=new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(file));
            try {
                String s;
                while ((s = in.readLine()) != null) {
                    str.add(s);
                }

            } finally {
                in.close();
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        int i=0;



        */

        ArrayList<Product> products=new ArrayList<>();

        Farmer farmer=new Farmer();
        farmer.setName("ООО Салтыковское");
        farmer.setDescription("В 2012 году в селе Оторма Земетченского района ООО АПК Родина  восстановила ферму на 400 голов дойного стада. В 2013 году на базе предприятия ООО ");
        farmer.setGeolocation("Земетчинский район, с. Салтыково");
        farmer.setIsStored(true);
/*
        product=new Product();
        product.setName("Молоко цельное пастеризованное (3,4-4%)");
        product.setProductDescription("Описание: Стадо хозяйства насчитывает порядка 400 голов. ");
        product.setPrice(70.00f);
        //product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
        product=new Product();
        product.setName("Сметана 20%");
        product.setProductDescription("Описанhttp://localhost:8080/farmers/1/productsие: Для приготовления сметаны используют пастеризованные сливки нужной жирности и закваску. Затем продукт охлаждается и фасуется. ");
        product.setPrice(70.00f);
       // product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
        product=new Product();
        product.setName("Творог деревенский");
        product.setProductDescription("Описание: В обезжиренную молочную смесь вносится закваска и хлористый кальций (для отделения творога от молочного белка, для обогащения кальцием). ");
        product.setPrice(95.00f);
       // product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
        product=new Product();
        product.setName("Колбаса «Армавирская»");
        product.setProductDescription("Описание: В благородстве вкуса, насыщенности и разнообразии ароматов не уступят самым изысканным деликатесам, оказавшись пососедству с ними на праздничном столе. ");
        product.setPrice(270.00f);
        //product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
        product=new Product();
        product.setName("Колбаса \"Сервелат\"");
        product.setProductDescription("Описание: Сервелат - варёно-копчёная колбаса, которая изготавливается из отборного мяса и обладает характерным, ");
        product.setPrice(270.00f);
       // product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
        product=new Product();
        product.setName("Куры для жарки (вес 1,2 – 2 кг) ");
        product.setProductDescription("Описание: Домашние куры выращиваются на воле и питаются исключительно натуральными кормами. ");
        product.setPrice(195.00f);
        //product.setFarmer(farmer);
        productService.addProduct(product);
        products.add(product);
*/
        ArrayList<Product> products2 =new ArrayList<>(Arrays.asList(
                new Product("Молоко", "frere/dfds", 4.0f, "Сервелат - варёно-копчёная колбаса", 45.2f, null, null, null),
                new Product("Куры для жарки", "sdfs", 3.0f, "Домашние куры", 45.9f, null, null, null)));


        //farmer.setProducts(products2);



        farmerRepository.save(farmer);
        products2.forEach(e -> e.setFarmer(farmer));


        products2.forEach(e -> productRepository.save(e));
//        farmer.setProducts(products);
//        farmerRepository.save(farmer);
//        farmerService.addFarmer(farmer);

//        products2.forEach(e -> productService.addProduct(e));
//        int x=1;
//        farmer.setProducts(products2);
//        farmerService.addFarmer(farmer);


        /*
        while (i<str.size()){
            product=new Product();
            product.setName(str.get(i));
            product.setProductDescription(str.get(i + 1));
            product.setPrice(Float.valueOf(str.get(i + 2)));
            System.out.println(productService.addProduct(product));
            i=i+3;
        }
*/
    }
}
