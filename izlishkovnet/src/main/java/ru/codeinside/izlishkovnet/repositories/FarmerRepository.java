package ru.codeinside.izlishkovnet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.codeinside.izlishkovnet.models.Farmer;

/**
 * Created by siphilis on 25.07.15.
 */
public interface FarmerRepository extends PagingAndSortingRepository<Farmer, Long>
{
}
