package ru.codeinside.izlishkovnet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.codeinside.izlishkovnet.models.Order;

/**
 * Created by siphilis on 26.07.15.
 */
public interface OrderRepository extends PagingAndSortingRepository<Order, Long>
{
}
