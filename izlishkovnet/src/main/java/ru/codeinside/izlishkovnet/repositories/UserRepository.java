package ru.codeinside.izlishkovnet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import ru.codeinside.izlishkovnet.models.User;

/**
 * Created by siphilis on 24.07.15.
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long>
{
    public User findByEmail(String email);

}
