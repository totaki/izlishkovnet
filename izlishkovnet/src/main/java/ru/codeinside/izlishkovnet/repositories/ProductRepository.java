package ru.codeinside.izlishkovnet.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.codeinside.izlishkovnet.models.Product;

import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */

public interface ProductRepository extends PagingAndSortingRepository<Product, Long>
{
    List<Product> findAll();
}
