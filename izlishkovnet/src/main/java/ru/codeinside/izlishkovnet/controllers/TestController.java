package ru.codeinside.izlishkovnet.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.codeinside.izlishkovnet.models.*;
import ru.codeinside.izlishkovnet.services.*;

/**
 * Created by siphilis on 24.07.15.
 */
@RestController
public class TestController
{

        @Autowired
        UserService userService;
    @Autowired
    CustomerService customerService;
    @Autowired
    FarmerService farmerService;
    @Autowired
    KindService kindService;
    @Autowired
    ProductService productService;
    @Autowired
    OrderService orderService;
    @Autowired
    FarmerPhotoService farmerPhotoService;

    @RequestMapping("/tcustomer")
    public String testCustomer()
    {
        return "customer";
    }

    @RequestMapping("/tfarmer")
    public String testFarmer()
    {
        return "farmer";
    }

    @RequestMapping("/tkind")
    public String testKind()
    {
        return "kind";
    }
/*
    @RequestMapping("/tproduct")
    public String testProduct()
    {
        //return productService.getAllProduct().toString();

/*
        TestEnum.UserLogin.validate("dfdf");
*/
    //    return "product";

   // }

    @RequestMapping("/torder")
    public String testOrder()
    {
        return "order";
    }

}
