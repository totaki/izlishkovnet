package ru.codeinside.izlishkovnet.config;

import org.springframework.context.annotation.Bean;
import ru.codeinside.izlishkovnet.models.FarmerPhoto;
import ru.codeinside.izlishkovnet.services.*;
import ru.codeinside.izlishkovnet.services.impls.*;

/**
 * Created by siphilis on 25.07.15.
 */
public class ConfigService
{
    @Bean
    public UserService userService()
    {
        return new UserServiceImpl();
    }

    @Bean
    public CustomerService customerService()
    {
        return new CustomerServiceImpl();
    }

    @Bean
    FarmerService farmerService()
    {
        return new FarmerServiceImpl();
    }

    @Bean
    KindService kindService()
    {
        return new KindServiceImpl();
    }

    @Bean
    ProductService productService()
    {
        return new ProductServiceImpl();
    }

    @Bean
    OrderService orderService()
    {
        return new OrderServiceImpl();
    }

    @Bean
    FarmerPhotoService farmerPhotoService()
    {
        return new FarmerPhotoServiceImpl();
    }

}
