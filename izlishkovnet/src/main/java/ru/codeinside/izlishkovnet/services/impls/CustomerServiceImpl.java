package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Customer;
import ru.codeinside.izlishkovnet.repositories.CustomerRepository;
import ru.codeinside.izlishkovnet.services.CustomerService;

import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
public class CustomerServiceImpl implements CustomerService
{
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> getAllCustomers()
    {
        return (List<Customer>)customerRepository.findAll();
    }

    public Long addCustomer(Customer newCustomer)
    {
        return customerRepository.save(newCustomer).getId();
    }

    public Customer getCustomerById(Long id)
    {
        return customerRepository.findOne(id);
    }
}
