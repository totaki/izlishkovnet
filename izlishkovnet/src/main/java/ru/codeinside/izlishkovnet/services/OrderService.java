package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.Order;

/**
 * Created by siphilis on 26.07.15.
 */
public interface OrderService
{
    public Long addOrder(Order newOrder);

    public Long updateOrder(Order modifiedOrder);

    public Order getOrderById(Long id);
}
