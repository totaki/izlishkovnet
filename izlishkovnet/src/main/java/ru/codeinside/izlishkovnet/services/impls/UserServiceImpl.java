package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.User;
import ru.codeinside.izlishkovnet.repositories.UserRepository;
import ru.codeinside.izlishkovnet.services.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by siphilis on 24.07.15.
 */
public class UserServiceImpl implements UserService
{
    @Autowired
    UserRepository userRepository;

    private Long saveUser(User user)
    {
        return userRepository.save(user).getId();
    }

    public List<User> getAllUsers()
    {
        return (ArrayList<User>)userRepository.findAll();
    }

    public Long addUser(User newUser)
    {
        return saveUser(newUser);
    }

    public User getUserByEmail(String email)
    {
        return userRepository.findByEmail(email);
    }

    public User getUserById(Long id)
    {
        return userRepository.findOne(id);
    }

    public Long updateUser(User modifiedUser)
    {
        return saveUser(modifiedUser);
    }
}
