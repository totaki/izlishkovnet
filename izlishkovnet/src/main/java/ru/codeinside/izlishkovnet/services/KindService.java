package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.Kind;

/**
 * Created by siphilis on 25.07.15.
 */
public interface KindService
{
    public Long addKind(Kind newKind);

    public Kind getKindById(Long id);

    public Long updateKind(Kind modifiedKind);
}
