package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Farmer;
import ru.codeinside.izlishkovnet.models.FarmerPhoto;
import ru.codeinside.izlishkovnet.repositories.FarmerPhotoRepository;
import ru.codeinside.izlishkovnet.services.FarmerPhotoService;

/**
 * Created by siphilis on 26.07.15.
 */
public class FarmerPhotoServiceImpl implements FarmerPhotoService
{
    @Autowired
    FarmerPhotoRepository farmerPhotoRepository;

    private Long saveFarmerPhoto(FarmerPhoto farmerPhoto)
    {
        return farmerPhotoRepository.save(farmerPhoto).getId();
    }

    public Long addFarmerPhoto(FarmerPhoto newFarmerPhoto)
    {
        return saveFarmerPhoto(newFarmerPhoto);
    }

    public Long updateFarmerPhoto(FarmerPhoto modifiedFarmerPhoto)
    {
        return saveFarmerPhoto(modifiedFarmerPhoto);
    }

    public FarmerPhoto getFarmerPhotoById(Long id)
    {
        return farmerPhotoRepository.findOne(id);
    }
}
