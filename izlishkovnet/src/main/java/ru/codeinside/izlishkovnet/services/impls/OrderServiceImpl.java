package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Order;
import ru.codeinside.izlishkovnet.repositories.OrderRepository;
import ru.codeinside.izlishkovnet.services.OrderService;

/**
 * Created by siphilis on 26.07.15.
 */
public class OrderServiceImpl implements OrderService
{
    @Autowired
    OrderRepository orderRepository;

    private Long saveOrder(Order order)
    {
        return orderRepository.save(order).getId();
    }

    public Long addOrder(Order newOrder)
    {
        return saveOrder(newOrder);
    }

    public Long updateOrder(Order modifiedOrder)
    {
        return saveOrder(modifiedOrder);
    }

    public Order getOrderById(Long id)
    {
        return orderRepository.findOne(id);
    }
}
