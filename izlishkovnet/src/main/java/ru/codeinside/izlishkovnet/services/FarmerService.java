package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.Farmer;

/**
 * Created by siphilis on 25.07.15.
 */
public interface FarmerService
{
    public Long addFarmer(Farmer newFarmer);

    public Farmer getFarmerById(Long id);
}
