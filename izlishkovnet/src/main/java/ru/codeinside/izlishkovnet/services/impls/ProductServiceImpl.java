package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Product;
import ru.codeinside.izlishkovnet.repositories.ProductRepository;
import ru.codeinside.izlishkovnet.services.ProductService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
public class ProductServiceImpl implements ProductService
{
    @Autowired
    ProductRepository productRepository;

    private Long saveProduct(Product product)
    {
        return productRepository.save(product).getId();
    }

    public Long addProduct(Product newProduct)
    {
        return saveProduct(newProduct);
    }

    public Long updateProduct(Product modifiedProduct)
    {
        return saveProduct(modifiedProduct);
    }

    public Product getProductById(Long id)
    {
        return productRepository.findOne(id);
    }

    public List<Product> getAllProduct(){
        return (ArrayList<Product>)productRepository.findAll();
    }
}
