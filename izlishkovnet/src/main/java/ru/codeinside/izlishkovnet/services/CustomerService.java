package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.Customer;

import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
public interface CustomerService
{
    public List<Customer> getAllCustomers();

    public Long addCustomer(Customer newCustomer);

    public Customer getCustomerById(Long id);
}
