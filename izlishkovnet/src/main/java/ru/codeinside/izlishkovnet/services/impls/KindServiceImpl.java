package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Kind;
import ru.codeinside.izlishkovnet.repositories.KindRepository;
import ru.codeinside.izlishkovnet.services.KindService;

/**
 * Created by siphilis on 25.07.15.
 */
public class KindServiceImpl implements KindService
{
    @Autowired
    KindRepository kindRepository;

    private Long saveKind(Kind kind)
    {
        return kindRepository.save(kind).getId();
    }

    public Long addKind(Kind newKind)
    {
        return saveKind(newKind);
    }

    public Long updateKind(Kind modifiedKind)
    {
        return saveKind(modifiedKind);
    }

    public Kind getKindById(Long id)
    {
        return kindRepository.findOne(id);
    }
}
