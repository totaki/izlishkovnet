package ru.codeinside.izlishkovnet.services.impls;

import org.springframework.beans.factory.annotation.Autowired;
import ru.codeinside.izlishkovnet.models.Farmer;
import ru.codeinside.izlishkovnet.repositories.FarmerRepository;
import ru.codeinside.izlishkovnet.services.FarmerService;

/**
 * Created by siphilis on 25.07.15.
 */
public class FarmerServiceImpl implements FarmerService
{
    @Autowired
    FarmerRepository farmerRepository;


    public Long addFarmer(Farmer newFarmer)
    {
        return farmerRepository.save(newFarmer).getId();
    }

    public Farmer getFarmerById(Long id)
    {
        return farmerRepository.findOne(id);
    }
}
