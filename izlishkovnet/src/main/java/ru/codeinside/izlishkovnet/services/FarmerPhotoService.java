package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.FarmerPhoto;

/**
 * Created by siphilis on 26.07.15.
 */
public interface FarmerPhotoService
{
    public Long addFarmerPhoto(FarmerPhoto newFarmerPhoto);

    public Long updateFarmerPhoto(FarmerPhoto modifiedFarmerPhoto);

    public FarmerPhoto getFarmerPhotoById(Long id);
}
