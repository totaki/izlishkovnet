package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.User;

import java.util.List;

/**
 * Created by siphilis on 24.07.15.
 */
public interface UserService
{
    public List<User> getAllUsers();

    public Long addUser(User newUser);

    public User getUserByEmail(String email);

    public User getUserById(Long id);

    public Long updateUser(User modifiedUser);
}
