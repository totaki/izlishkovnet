package ru.codeinside.izlishkovnet.services;

import ru.codeinside.izlishkovnet.models.Product;

import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
public interface ProductService
{
    public Long addProduct(Product newProduct);

    public Long updateProduct(Product modifiedProduct);

    public Product getProductById(Long productId);

    public List<Product> getAllProduct();

}
