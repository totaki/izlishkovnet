package ru.codeinside.izlishkovnet.models;

import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;

/**
 * Created by siphilis on 26.07.15.
 */
@Entity
@Table(name = "farmer_photos")
public class FarmerPhoto
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "farmer_photo_id")
    private Long id;
    public Long getId()
    {
        return id;
    }

    //----------------------------------------------------------------------

    @Column(name = "photo_path")
    private String photoPath;
    public String getPhotoPath()
    {
        return photoPath;
    }
    public void setPhotoPath(String photoPath)
    {
        this.photoPath = RegularExpression.path.validate(photoPath)? photoPath : "hasNoValue";
    }

    //----------------------------------------------------------------------

    @ManyToOne
    @JoinColumn(name = "farmer_id")
    private Farmer farmer;//Are getters/setters necessary here???????????????????????????
   /* public Farmer getFarmer()
    {
        return farmer;
    }
    public void setFarmer(Farmer farmer)
    {
        this.farmer = farmer;
    }*/

    //------------------------------------------------------------------------

    public FarmerPhoto(){}

    //---------------------------------------------------------------------------

    public FarmerPhoto(String photoPath, Farmer farmer)
    {
        setPhotoPath(photoPath);
        this.farmer = farmer;
    }
}
