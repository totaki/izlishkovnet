package ru.codeinside.izlishkovnet.models;

import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by siphilis on 24.07.15.
 */
@Entity
@Table(name = "users")
public class User
{
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    public Long getId()
    {
        return id;
    }

    //-------------------------------------------------------------------------

    @Column(name = "email", unique = true, nullable = false)
    private String email;
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = RegularExpression.email.validate(email)? email : "hasNoValue";
    }

    //-------------------------------------------------------------------------

    @Column(name = "address")
    private String address;
    public String getAddress()
    {
        return address;
    }
    public void setAddress(String address)
    {
        this.address = RegularExpression.address.validate(address)? address : "hasNoValue";
    }

    //---------------------------------------------------------------------------

    @Column(name = "phone_number")
    private String phoneNumber;
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = RegularExpression.phoneNumber.validate(phoneNumber)? phoneNumber : "hasNoValue";
    }

    //-------------------------------------------------------------------------------

    @Column(name = "created_at", updatable = false)
    private Date createdAt = new Date();
    public Date getCreatedAt()
    {
        return createdAt;
    }

    //---------------------------------------------------------------------------------

    @Column(name = "visited_at")
    private Date visitedAt = new Date();
    public Date getVisitedAt()
    {
        return visitedAt;
    }
    public void setVisitedAt(Date visitedAt)
    {
        this.visitedAt = visitedAt;
    }

    //---------------------------------------------------------------------------------

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Customer customer;
    public Customer getCustomer()
    {
        return customer;
    }
    public void setCustomer(Customer customer)
    {
        if (this.farmer != null)
        {
            throw new IllegalStateException("You tried to put client into farmer");
        }
        this.customer = customer;
    }

    //--------------------------------------------------------------------------------

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Farmer farmer;
    public Farmer getFarmer()
    {
        return farmer;
    }
    public void setFarmer(Farmer farmer)
    {
        if (this.customer != null)
        {
            throw new IllegalStateException("You tried to put farmer into client");
        }
        this.farmer = farmer;
    }

    //------------------------------------------------------------------------------------
    public User(){}

    //-------------------------------------------------------------------------------------

    public User(String email, String phoneNumber, String address)
    {
        setEmail(email);
        setPhoneNumber(phoneNumber);
        setAddress(address);
        setVisitedAt(new Date());
    }

}