package ru.codeinside.izlishkovnet.models;

/**
 * Created by siphilis on 27.07.15.
 */
public enum RegularExpression
{
    id("[0-9]+"),
    name("^.{1,256}$"),
    email("^([a-z0-9_\\.-]+)@([a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$"),
    address("^.{1,256}$"),
    phoneNumber("^.{1,256}$"),
    rating("^(?:[1-9]\\d*|0)?(?:\\.\\d+)?$"),
    price("^(?:[1-9]\\d*|0)?(?:\\.\\d+)?$"),
    count("^(?:[1-9]\\d*|0)?(?:\\.\\d+)?$"),
    integer_number("[1-9]([0-9]{,30})"),
    description("^.{1,256}$"),
    path("^.{1,256}$"),
    geolocation("^.{1,256}$"),
    orderStatus("(considered)|(inProgress)|(completed)");

    private final String regex;

    RegularExpression(String regex)
    {
        this.regex = regex;
    }

    private Boolean matchWithRegex(String expression)
    {
        if (expression == null)
            return false;
        else
            return expression.matches(regex);
    }

    public Boolean validate(Float expression)
    {
        return matchWithRegex(expression.toString());
    }

    public Boolean validate(String expression)
    {
        return matchWithRegex(expression);
    }

    public Boolean validate(Integer expression)
    {
        return matchWithRegex(expression.toString());
    }

    public Boolean validate(OrderStatus expression)
    {
        return matchWithRegex(expression.name());
    }
}
