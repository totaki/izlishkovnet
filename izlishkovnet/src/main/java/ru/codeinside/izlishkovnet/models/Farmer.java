package ru.codeinside.izlishkovnet.models;

import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;
import java.util.List;

//* Created by siphilis on 24.07.15.


@Entity
@Table(name = "farmers")
public class Farmer
{
    @Id
    @Column(name = "farmer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    public Long getId()
    {
        return id;
    }

    //---------------------------------

    @Column(name = "farmer_name")
    private String name;
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name =  RegularExpression.name.validate(name)? name : "hasNoValue";
    }

    //---------------------------------

    @Column(name = "farmer_description")
    private String description;
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = RegularExpression.description.validate(description)? description : "hasNoValue";

    }

    //-----------------------------------

    @Column(name = "cover_path")
    private String coverPath;
    public String getCoverPath()
    {
        return coverPath;
    }
    public void setCoverPath(String coverPath)
    {
        this.coverPath = RegularExpression.path.validate(coverPath)? coverPath : "hasNoValue";
    }

    //-------------------------------------

    @Column(name = "geolocation")
    private String geolocation;
    public String getGeolocation()
    {
        return geolocation;
    }
    public void setGeolocation(String geolocation)
    {
        this.geolocation = RegularExpression.geolocation.validate(geolocation)? geolocation : "hasNoValue";
    }
    //---------------------------------------

    @Column(name = "is_delivered")
    private Boolean isDelivered;
    public Boolean getIsDelivered()
    {
        return isDelivered;
    }
    public void setIsDelivered(Boolean isDelivered)
    {
        this.isDelivered = isDelivered;
    }

    //-------------------------------------

    @Column(name = "is_stored")
    private Boolean isStored;
    public Boolean getIsStored()
    {
        return isStored;
    }
    public void setIsStored(Boolean isStored)
    {
        this.isStored = isStored;
    }

    //--------------------------------------

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() { return user; }
    public void setUser(User user)
    {
        this.user = user;
    }

    //----------------------------------------

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "farmer", cascade = CascadeType.ALL)
    private List<Product> products;
    public List<Product> getProducts()
    {
        return products;
    }
    public void setProducts(List<Product> products)
    {
        this.products = products;
    }

    //------------------------------------------------------------------------------

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "farmer", cascade = CascadeType.ALL)
    private List<FarmerPhoto> photos;
    public List<FarmerPhoto> getPhotos()
    {
        return photos;
    }
    public void setPhotos(List<FarmerPhoto> photos)
    {
        this.photos = photos;
    }

    //-------------------------------------------------------------------------------

    public Farmer(){}

    //---------------------------------------

    public Farmer(String name, String description, String coverPath, String geolocation, Boolean isDelivered,
                  Boolean isStored, User user, List<Product> products, List<FarmerPhoto> photos)
    {
        setName(name);
        setDescription(description);
        setCoverPath(coverPath);
        setGeolocation(geolocation);
        setIsDelivered(isDelivered);
        setIsStored(isStored);
        setUser(user);
        setProducts(products);
        setPhotos(photos);
    }

    //-----------------------------------------------------------------------------

    public void addProducts(List<Product> products)
    {
        products.forEach(p -> this.products.add(p));
    }

    //-----------------------------------------------------------------------------

    public void addPhotos(List<FarmerPhoto> photos)
    {
        photos.forEach(p -> this.photos.add(p));
    }

}
