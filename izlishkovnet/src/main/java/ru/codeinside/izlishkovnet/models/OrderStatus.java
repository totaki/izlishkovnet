package ru.codeinside.izlishkovnet.models;

/**
 * Created by siphilis on 26.07.15.
 */
public enum OrderStatus
{
    considered,
    inProgress,
    completed;
}
