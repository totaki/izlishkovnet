package ru.codeinside.izlishkovnet.models;

import ru.codeinside.izlishkovnet.utils.Utils;

import javax.annotation.RegEx;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by siphilis on 26.07.15.
 */
@Entity
@Table(name = "orders")
public class Order
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id")
    private Long id;
    public Long getId()
    {
        return id;
    }

    //---------------------------------------------------------------------

    @Column(name = "count", columnDefinition = "float default 1.0")
    private Float count;
    public Float getCount()
    {
        return count;
    }
    public void setCount(Float count)
    {
        this.count = RegularExpression.count.validate(count)? count : 0.0f;
    }

    //----------------------------------------------------------------------
    @Column(name = "price")
    private Float price;
    public Float getPrice()
    {
        return price;
    }
    public void setPrice(Float price)
    {
        this.price = RegularExpression.price.validate(price)? price : 0.0f;
    }

    //----------------------------------------------------------------------

    @Column(name = "status")
    @Enumerated(EnumType.ORDINAL)
    private OrderStatus status = OrderStatus.considered;
    public OrderStatus getStatus()
    {
        return status;
    }
    public void setStatus(OrderStatus status)
    {
        this.status = RegularExpression.orderStatus.validate(status)? status : OrderStatus.considered;//!!!!!!
    }

    //----------------------------------------------------------------------

    @Column(name = "total_cost")
    private Float totalCost;
    private void setTotalCost()// NB!!!!!!!!!!!!!!!
    {
        this.totalCost = this.price * this.count;
    }
    public Float getTotalCost()
    {
        return totalCost;
    }

    //-------------------------------------------------------------------------

    @Column(name = "registred_at", updatable = false)
    private Date registredAt;
    public Date getRegistredAt()
    {
        return registredAt;
    }
    public void setRegistredAt(Date registredAt)
    {
        this.registredAt = registredAt;
    }

    //------------------------------------------------------------------------

    @Column(name = "completed_at", updatable = false)
    private Date completedAt;
    public Date getCompletedAt()
    {
        return getCompletedAt();
    }
    public void setCompletedAt(Date completedAt)
    {
        this.completedAt = completedAt;
    }

    //----------------------------------------------------------------------

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    public Product getProduct()
    {
        return product;
    }
    public void setProduct(Product product)
    {
        this.product = product;
    }

    //----------------------------------------------------------------------

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    public Customer getCustomer()
    {
        return customer;
    }
    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

    //-------------------------------------------------------------------------

    public Order() {}

    //-------------------------------------------------------------------------

    public Order(Product product, Customer customer)
    {
        setProduct(product);
        setCustomer(customer);
        setPrice(this.product.getPrice());//Measure???????????????????
    }

    //---------------------------------------------------------------------------

    public void registerOrder(Float count)
    {
        setRegistredAt(new Date());
        setCount(count);
        setTotalCost();
        setStatus(OrderStatus.inProgress);
    }
}
