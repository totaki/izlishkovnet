package ru.codeinside.izlishkovnet.models;

import org.springframework.validation.annotation.Validated;
import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;
import java.util.List;

/**
 * Created by siphilis on 24.07.15.
 */
@Entity
@Table(name = "customers")
public class Customer
{
    @Id
    @Column(name = "customer_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    public Long getId()
    {
        return id;
    }

    //------------------------------------------------------

    @Column(name = "rating", columnDefinition = "float default 0.0")
    private Float rating;
    public Float getRating() { return rating; }
    public void setRating(Float rating)
    {
        this.rating = RegularExpression.rating.validate(rating)? rating : 0.0f;
    }

    //-------------------------------------------------------

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    public User getUser() { return user; }
    public void setUser(User user)
    {
        this.user = user;
    }

    //---------------------------------------------------------

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customer", cascade = CascadeType.ALL)
    private List<Order> orders;
    public List<Order> getOrders()
    {
        return orders;
    }
    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

    //---------------------------------------------------------

    public Customer() {}

    //---------------------------------------------------------
    public Customer(Float rating, User user, List<Order> orders)
    {
        setRating(rating);
        setUser(user);
        setOrders(orders);
    }

    //---------------------------------------------------------

    public void addOrders(List<Order> orders)
    {
        orders.forEach(o -> this.orders.add(o));
    }
}
