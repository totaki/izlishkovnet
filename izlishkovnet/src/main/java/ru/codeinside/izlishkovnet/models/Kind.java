package ru.codeinside.izlishkovnet.models;

import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;
import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
@Entity
@Table(name = "kinds")
public class Kind
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "kind_id")
    private Long id;
    public Long getId()
    {
        return id;
    }

    //-------------------------------------------------

    @Column(name = "kind_name", unique = true, nullable = false)
    private String name;
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = RegularExpression.name.validate(name)? name : "hasNoValue";
    }

    //---------------------------------------------------

    @Column(name = "logo_path")
    private String logoPath;
    public String getLogoPath()
    {
        return logoPath;
    }
    public void setLogoPath(String logoPath)
    {
        this.logoPath = RegularExpression.path.validate(logoPath)? logoPath : "hasNoValue";
    }

    //----------------------------------------------------

    @Column(name = "level")
    private Integer kindLevel;
    public Integer getKindLevel()
    {
        return kindLevel;
    }
    public void setKindLevel(Integer kindLevel)
    {
        this.kindLevel = RegularExpression.integer_number.validate(kindLevel)? kindLevel : 0;;
    }

    //---------------------------------------------------

    @Column(name = "parent_id")
    private Long parentId;
    public Long getParentId()
    {
        return parentId;
    }

    //-----------------------------------------------------

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    private Kind parent;
    public Kind getParent()
    {
        return parent;
    }
    public void setParent(Kind parent)
    {
        this.parent = parent;
    }

    //-----------------------------------------------------

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kind")
    private List<Product> products;
    public List<Product> getProducts()
    {
        return products;
    }
    public void setProducts(List<Product> products)
    {
        this.products = products;
    }

    //--------------------------------------------------------

    public Kind(){}

    //-----------------------------------------------------

    private void initialConstructor(String name, String logoPath)
    {
        setName(name);
        setLogoPath(logoPath);
    }

    //-----------------------------------------------------

    public Kind(String name, String logoPath, Kind parent,List<Product> products)
    {
        initialConstructor(name, logoPath);
        setParent(parent);
        setKindLevel(1 + this.parent.getKindLevel());
        setProducts(products);
    }

    //-----------------------------------------------------

    public Kind(String name, String logoPath)
    {
        initialConstructor(name, logoPath);
        setKindLevel(0);
    }

    //-----------------------------------------------------

    public void addProducts(List<Product> products)
    {
        products.forEach(p -> this.products.add(p));
    }
}
