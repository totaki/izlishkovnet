package ru.codeinside.izlishkovnet.models;


import ru.codeinside.izlishkovnet.utils.Utils;

import javax.persistence.*;
import java.util.List;

/**
 * Created by siphilis on 25.07.15.
 */
@Entity
@Table(name = "products")
public class Product
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id")
    private Long id;
    public Long getId()
    {
        return id;
    }

    //---------------------------------------------------------------

    @Column(name = "product_name")
    private String name;
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = RegularExpression.name.validate(name)? name : "hasNoValue";
    }

    //---------------------------------------------------------------

    @Column(name = "photo_path")
    private String photoPath;
    public String getPhotoPath()
    {
        return photoPath;
    }
    public void setPhotoPath(String photoPath)
    {
        this.photoPath = RegularExpression.path.validate(photoPath)? photoPath : "hasNoValue";
    }

    //---------------------------------------------------------------

    @Column(name = "rating", columnDefinition = "float default 0.0")
    private Float rating;
    public Float getRating()
    {
        return rating;
    }
    public void setRating(Float rating)
    {
        this.rating = RegularExpression.rating.validate(rating)? rating : 0.0f;
    }

    //----------------------------------------------------------------

    @Column(name = "description")
    private String description;
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = RegularExpression.description.validate(description)? description : "hasNoValue";
    }

    //-----------------------------------------------------------------

    @Column(name = "price", columnDefinition = "float default 0.0")
    private Float price;
    public Float getPrice()
    {
        return price;
    }
    public void setPrice(Float price)
    {
        this.price = RegularExpression.price.validate(price)? price : 0.0f;
    }

    //-----------------------------------------------------------------

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "farmer_id")
    Farmer farmer;
    public Farmer getFarmer()
    {
        return farmer;
    }
    public void setFarmer(Farmer farmer)
    {
        this.farmer = farmer;
    }

    //-----------------------------------------------------------------

    @ManyToOne
    @JoinColumn(name = "kind_id")
    Kind kind;
    public Kind getKind()
    {
        return kind;
    }
    public void setKind(Kind kind)
    {
        this.kind = kind;
    }

    //-----------------------------------------------------------------

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "product")
    List<Order> orders;
    public List<Order> getOrders()
    {
        return orders;
    }
    public void setOrders(List<Order> orders)
    {
        this.orders = orders;
    }

    //-----------------------------------------------------------------

    public Product() {}

    //----------------------------------------------------------------

    public Product(String name, String photoPath, Float rating, String description, Float price,
                   Farmer farmer, Kind kind, List<Order> orders)
    {
        setName(name);
        setPhotoPath(photoPath);
        setRating(rating);
        setDescription(description);
        setPrice(price);
        setFarmer(farmer);
        setKind(kind);
        setOrders(orders);
    }

    //-----------------------------------------------------------------

    public void addOrders(List<Order> orders)
    {
        orders.forEach(o -> this.orders.add(o));
    }

}
