/*
package utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

*/
/**
 * Created by siphilis on 27.07.15.
 *//*

@RunWith(Parameterized.class)
public class ParametrizedTest extends Assert
{

//    @Parameterized.Parameter
    public String message;

//    @Parameterized.Parameter(1)
    public int value;

    public ParametrizedTest(String message, int value)
    {
        this.message = message;
        this.value = value;
    }

    @Test
    public void testWithParams() throws Exception
    {
        assertTrue(message, 3 > value);
    }

    @Parameterized.Parameters(name = "{index}:{0}-{1}")
    public static List<Object> getParameters() {
        return Arrays.asList(
                new Object[] {"mwssag 1", 1},
                new Object[] {"mwssag 2", 2},
                new Object[] {"mwssag 3", 3},
                new Object[] {"mwssag 4", 4}
        );
    }
}
*/
